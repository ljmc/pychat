#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import selectors
import socket
import time


class ChatServer(object):
    '''docstring for ChatServer.'''
    def __init__(self, HOST, PORT):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind(('', PORT))
        self.server_socket.listen(5)
        self.server_socket.setblocking(False)

        self.sockets = [self.server_socket]

        self.selector = selectors.DefaultSelector()
        self.selector.register(self.server_socket, selectors.EVENT_READ,
                               self.add_connection)
        print('Chat server started on port {}'.format(PORT))
        return

    def run(self):
        '''Main method, has the infinite loop of the ChatServer'''
        while True:
            select_tuple = self.selector.select()
            for key, event in select_tuple:
                callback = key.data
                callback(key.fileobj, event)
        return

    def receive(self, sock, mask):
        '''Receiver method, activate an effector method'''
        try:
            data = sock.recv(RECV_BUFFER)
            data = data.decode('utf-8')
            if data:
                # self.echo_up('{}'.format(data), sock)
                self.broadcast(data, sock)
        except:
            self.remove_connection(sock)
            pass
        return

    def echo_up(self, message, origin):
        '''Effector method, echos a capitallised message back to the sender'''
        message = message.upper()
        try:
            origin.sendall(message.encode('utf-8'))
        except:
            self.remove_connection(origin)
        return

    def broadcast(self, message, origin):
        '''Effector method, sends message to the all clients except sender'''
        addr = origin.getpeername()
        message = '<{}:{}> {}'.format(addr[0], addr[1], message)
        for sock in self.sockets:
            if sock != self.server_socket and sock != origin:
                try:
                    sock.sendall(message.encode('utf-8'))
                except:
                    self.remove_connection(sock)
        return

    def add_connection(self, sock, mask):
        '''Helper method, registers a new client to selector'''
        conn, addr = sock.accept()
        self.sockets.append(conn)
        self.selector.register(conn, selectors.EVENT_READ, self.receive)
        print('{} <{}:{}> joined'.format(
            time.strftime('%d/%m/%y - %H:%M:%S', time.localtime()),
            addr[0], addr[1])
        )
        conn.sendall('Welcome to pyChat.\n'.encode('utf-8'))
        self.echo_up('<{}:{}> joined'.format(addr[0], addr[1]), conn)
        return

    def remove_connection(self, sock):
        '''Helper methods, removes a dead/disconnected client from selector'''
        addr = sock.getpeername()
        print('{} <{}:{}> left'.format(
            time.strftime('%d/%m/%y - %H:%M:%S',
                          time.localtime()), addr[0], addr[1]))
        self.echo_up('<{}:{}> left'.format(addr[0], addr[1]), sock)
        sock.shutdown()
        sock.close()
        self.sockets.append(sock)
        self.selector.unregister(sock)
        return


if __name__ == '__main__':

    with open('config.json', 'r') as f_config:
        config = json.load(f_config)

    HOST, PORT = config['HOST'], config['PORT']

    RECV_BUFFER = 1024

    ChatServer(HOST, PORT).run()
