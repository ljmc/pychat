A simple python chatroom based on the sockets and selectors modules.

Telnet can be used as a client.

I haven't had time to do much testing, please do report any issues !

Currently, it works only on Unix based systems due to select being unable to handle sys.stdin on Windows.

Based on a few tutorials :    

https://www.binarytides.com/code-chat-application-server-client-sockets-python/    
https://www.ibm.com/developerworks/linux/tutorials/l-pysocks/    
http://www.bogotobogo.com/python/python_network_programming_tcp_server_client_chat_server_chat_client_select.php
https://pymotw.com/2/socket/tcp.html
