#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import json
import selectors
import socket
import sys


class ChatClient(object):
    """docstring for Client."""
    def __init__(self, HOST, PORT):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(5)
        print('Connecting to {}:{}'.format(HOST, PORT))
        try:
            self.socket.connect((HOST, PORT))
        except:
            print('Unable to connect')
            sys.exit()
        self.selector = selectors.DefaultSelector()
        self.selector.register(self.socket, selectors.EVENT_READ,
                               self.receive)
        self.selector.register(sys.stdin, selectors.EVENT_READ,
                               self.send)
        print('Connected')
        self.prompt()
        return

    def run(self):
        '''Main method, has the infinite loop of the ChatClient.'''
        while True:
            select_tuple = self.selector.select()
            for key, event in select_tuple:
                callback = key.data
                callback(key.fileobj, event)
        return

    def receive(self, sock, mask):
        '''Effector method, displays new message received from the server.'''
        try:
            data = sock.recv(RECV_BUFFER)
            print('data :', data)
            data = data.decode('utf-8')
            if data:
                sys.stdout.write(data)
        except:
            print('Disconnected from server')
            sys.exit()
        return

    def send(self, sock, mask):
        '''Effector method, sends new message to server.'''
        msg = sys.stdin.readline()
        self.socket.sendall(msg.encode('utf-8'))
        self.prompt()

    @staticmethod
    def prompt():
        '''Helper method, displays \'<You> \' in the terminal.'''
        sys.stdout.write('<You> ')
        sys.stdout.flush()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to pyChat help')
    parser.add_argument('-s', '--server', action='store', nargs='*',
                        help='specificy server and port to connect to')
    parser.add_argument('--buffer', action='store', nargs='?', default=1024,
                        help='specificy receive buffer (default=1024)')
    # TODO: add name to be displayed by ChatServer
    """
    parser.add_argument('-n', '--name', type=str, action='store', nargs='?',
                        help='specificy ')
    """
    args = parser.parse_args()

    try:
        HOST, PORT = args.server
    except:
        with open('config.json', 'r') as f_config:
            config = json.load(f_config)

        HOST, PORT = config['HOST'], config['PORT']

    try:
        RECV_BUFFER = args.buffer
    except:
        RECV_BUFFER = 1024

    ChatClient(HOST, PORT).run()
